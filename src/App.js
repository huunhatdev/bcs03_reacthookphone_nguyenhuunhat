import logo from "./logo.svg";
import "./App.css";
import BaiTapDienThoai from "./BaiTapDienThoai/BaiTapDienThoai";
function App() {
  return (
    <div className="p-3">
      <BaiTapDienThoai />
    </div>
  );
}

export default App;
