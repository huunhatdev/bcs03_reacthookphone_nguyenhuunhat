import { Button, Modal } from "antd";
import React from "react";

function Cart({
  cart,
  visible,
  setVisible,
  handleDelCart,
  handlePayment,
  handleQuantity,
}) {
  return (
    <Modal
      title="Giỏ hàng"
      centered
      visible={visible}
      onOk={() => {
        setVisible(false);
        handlePayment();
      }}
      onCancel={() => setVisible(false)}
      width={1000}
      okType="default"
      okText="Thanh toán"
    >
      <div className="relative overflow-x-auto shadow-md sm:rounded-lg">
        <table className="w-full text-sm text-left text-gray-500 ">
          <thead className="text-xs text-gray-700 uppercase bg-gray-50 ">
            <tr>
              <th scope="col" className="px-6 py-3">
                Tên sản phẩm
              </th>
              <th scope="col" className="px-6 py-3">
                Giá bán
              </th>
              <th scope="col" className="px-6 py-3">
                Số lượng
              </th>
              <th scope="col" className="px-6 py-3">
                Tổng
              </th>
              <th scope="col" className="px-6 py-3">
                <span className="sr-only">Edit</span>
              </th>
            </tr>
          </thead>
          <tbody>
            {cart.map((e, i) => {
              return (
                <tr className="bg-white border-b  hover:bg-gray-50 ">
                  <th
                    scope="row"
                    className="px-6 py-4 font-medium text-gray-900  whitespace-nowrap"
                  >
                    {e.tenSP}
                  </th>
                  <td className="px-6 py-4"> {e.giaBan}</td>
                  <td className="px-6 py-4">
                    <a
                      className="mx-2"
                      onClick={() => {
                        handleQuantity(e.maSP, -1);
                      }}
                    >
                      -
                    </a>
                    {e.quantity}
                    <a
                      className="mx-2"
                      onClick={() => {
                        handleQuantity(e.maSP, +1);
                      }}
                    >
                      +
                    </a>
                  </td>
                  <td className="px-6 py-4">{e.quantity * e.giaBan}</td>
                  <td className="px-6 py-4 text-right">
                    <a
                      className="font-medium text-red-500  hover:text-red-300"
                      onClick={() => handleDelCart(e.maSP)}
                    >
                      Delete
                    </a>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </Modal>
  );
}
export default React.memo(Cart);
