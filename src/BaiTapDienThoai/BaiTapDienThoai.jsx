import React, { useState, useCallback, useEffect, useMemo } from "react";
import Item from "./Item";
import dataPhone from "./data";
import Detail from "./Detail";
import { Button } from "antd";
import Cart from "./Cart";

export default function BaiTapDienThoai() {
  const [data, setData] = useState(dataPhone);
  const [detail, setDetail] = useState({});
  const [cart, setCart] = useState([]);
  const [visible, setVisible] = useState(true);

  const handleDetail = useCallback((maSP) => {
    let i = data.findIndex((e) => e.maSP === maSP);
    if (i !== -1) {
      setDetail(data[i]);
    }
  }, []);
  const handleAddToCart = useCallback(
    (product) => {
      let cloneCart = [...cart];
      if (cloneCart.length > 0) {
        let i = cloneCart.findIndex((e) => e.maSP === product.maSP);
        if (i === -1) {
          cloneCart.push({ ...product, quantity: 1 });
          setCart(cloneCart);
        } else {
          cloneCart[i].quantity += 1;
          setCart(cloneCart);
        }
      } else {
        cloneCart.push({ ...product, quantity: 1 });
        setCart(cloneCart);
      }
    },
    [cart]
  );
  const handleQuantity = useCallback(
    (maSP, quan) => {
      let cloneCart = [...cart];
      if (cloneCart.length > 0) {
        let i = cloneCart.findIndex((e) => e.maSP === maSP);
        if (i !== -1) {
          if (cloneCart[i].quantity === 1 && quan === -1) {
            cloneCart.splice(i, 1);
          } else {
            cloneCart[i].quantity += quan;
          }
          setCart(cloneCart);
        }
      }
    },
    [cart]
  );

  const handleDelCart = useCallback(
    (maSP) => {
      let cloneCart = [...cart];
      let i = cloneCart.findIndex((e) => e.maSP === maSP);

      if (i !== -1) {
        cloneCart.splice(i, 1);
        setCart(cloneCart);
      }
    },
    [cart]
  );

  const countCart = useMemo(() => {
    return cart.length;
  }, [cart]);

  const setVisibleModal = useCallback(
    (bool) => {
      setVisible(bool);
    },
    [visible]
  );

  const handlePayment = useCallback(() => {
    setCart([]);
  }, [cart]);

  return (
    <div>
      <Cart
        visible={visible}
        cart={cart}
        setVisible={setVisibleModal}
        handleDelCart={handleDelCart}
        handlePayment={handlePayment}
        handleQuantity={handleQuantity}
      />
      <h2 className=" bg-slate-800 text-white text-center text-3xl p-2 mb-3">
        Cửa hàng điện thoại di động
      </h2>
      <div className="flex justify-end my-3">
        <Button type="primary" danger onClick={() => setVisibleModal(true)}>
          Giỏ hàng ({countCart})
        </Button>
      </div>
      <div className="flex justify-around">
        {data.map((e, i) => {
          return (
            <Item
              dataItem={e}
              key={i}
              handleDetail={handleDetail}
              handleAddToCart={handleAddToCart}
            />
          );
        })}
      </div>
      <h2 className=" bg-slate-800 text-white text-center text-3xl p-2 mb-3 mt-3">
        Chi tiết sản phẩm
      </h2>
      <Detail dataDetail={detail} />
    </div>
  );
}
